import restify from 'restify';
import is from 'is_js';
import redis from 'redis';

import routes from './routes';

const createServer = obj => obj.createServer();
const createClient = (obj, url) => obj.createClient({url: url});

export default class App {
    constructor(host = '0.0.0.0', port = 9090) {

    // Setup and initialize the server
        this.server = this.initServer(host, port);

    // Setup the httpclient
        this.initClient('http://0.0.0.0:8800/');

    // Loading all the plugins in one place
        this.loadPlugins();

    // This will attach and register all the routes
        this.attachRoutes();

    // This sets up Redis
        this.redisClient = this.setupRedis();

    // Testing call
        this.getURL('?q="hello"', () => false);

    // Starts the server
        this.start();
    }

    setupRedis() {
        return redis.createClient();
    }

    loadPlugins() {
        this.server.use(restify.bodyParser());
        this.server.use(restify.queryParser());
    }

    initServer(host, port) {
        this.host = host;
        this.port = port;
        return createServer(restify);
    }

    initClient(url) {
        this.client = createClient(restify, url);
        return this.client;
    }

    _getServerObject() {
        return this.server;
    }

    _getClientObject() {
        return this.client;
    }

    getURL(url, callback) {

        this.client.get('/', (err, req) => {
            console.log('error', err); // connection error
            req.on('result', function(err, res) {
                console.log('error', err); // connection error

                res.body = '';
                res.setEncoding('utf8');
                res.on('data', function(chunk) {
                    res.body += chunk;
                });

                res.on('end', function() {
                    // Redis Client
                    // redisClient.set('home', res.body, redis.print);
                });
            });
        });

    }

    attachRoutes() {
        // console.log(routes);
        routes.map(x => {
            if (is.include(x.method, 'GET')) {
                console.log(x.method, x.path);
            } else if(is.include(x.method, 'POST')) {
                console.log(x.method, 'is posting to', x.path);
            }
        });

        return true;
    }

    start() {
        this.server.pre(function (request, response, next) {
            request.log.info({ req: request }, 'REQUEST');
            next();
        });

        this.server.listen(
            this.port,
            () => console.log('%s listening at %s', this.host, this.port)
        );
    }
}
