import handlers from './handlers';

export default [
  {method: 'GET', path: '/', fn: handlers.default, label: 'default'},
  {method: 'GET', path: '/hello', fn: handlers.default, label: 'hello'},
  {method: 'POST', path: '/hello', fn: handlers.default, label: 'hello'}
];
